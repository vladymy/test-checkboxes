(function($){
	$(document).ready(function(){
		var data = ["cat", "dog", "snake"];
		init(data);
		$('body').on('submit', '.add_animal_form', function(e){ // WHEN ADD NEW ANIMALS
			var $this = $(this),
					data_input = $this.find('.new_animal'),
					new_data = data_input.val();
			update_data(new_data);
			data.push(new_data);
			data_input.val('');
			return false;
		})
		$('body').on("change", "input[type=checkbox]", function(e){  // WHEN CHECKBOX IS CHANGED
			var $this = $(this);
			switch($this.hasClass('select_all')) { 	// 
				default : 	// IF IT NOT SELECT ALL CHECKBOX
					check_checboxes();
					break;
				case true: // IF IT SELECT ALL CHECKBOX
					select_deselect_all_in($('.animals_box'));
					break;
			}
		});
	});

	// ADDITIONAL FUNCTIONS
	function init(data){
		var html='';
		for(var i=0; i < data.length; i++){
			html += '<div class="checkbox">\
				  <label>\
				    <input type="checkbox" class="" value="">\
				    '+data[i]+'\
				  </label>\
				</div>';
		}
		container = $('.animals_box').append(html);
	}
	function update_data(new_data){
		var html = '<div class="checkbox">\
				  <label>\
				    <input type="checkbox" class="" value="">\
				    '+new_data+'\
				  </label>\
				</div>';
		container = $('.animals_box').append(html);
		check_checboxes();
	}
	function check_checboxes(){
		var select_all_checkbox =	$(".select_all"),
				select_all_wrapper =	select_all_checkbox.closest('.select_all_wrapper');
		
		if( 	is_checked_all_in($('.animals_box')) 	){ // CHECK IS ALL CHECKBOXES WERE CHECKED
			select_all_wrapper.removeClass('selected_part');
			select_all_checkbox.prop( "checked", true); 
			return false;
		}
		if( 	is_any_checked_in($('.animals_box')) 	){ // CHECK IS ANY CHECKBOX WERE CHECKED
			select_all_wrapper.addClass('selected_part');
			select_all_checkbox.prop( "checked", false);
			return false;
		}
		select_all_wrapper.removeClass('selected_part');
		select_all_checkbox.prop( "checked", false);
	}
	function is_any_checked_in(container){
		var status;
		container.find("input[type=checkbox]").each(function(){
			if ($(this).prop("checked") == true) {
				status = true;
				return false;
			}else{
				status=false;
			}
		})
		return status;
	}
	function is_checked_all_in(container){
		var status;
		container.find("input[type=checkbox]").each(function(){
			if ($(this).prop("checked") == false) {
				status = false;
				return false;
			}else{
				status=true;
			}
		})
		return status;
	}
	function select_deselect_all_in(container){
		var select_all_checkbox = $(".select_all"),
				select_all_wrapper =	select_all_checkbox.closest('.select_all_wrapper');
		if( 	is_checked_all_in(container) 	){													 // if selected all - unselect all
			container.find("input[type=checkbox]").prop( "checked", false);
			select_all_wrapper.removeClass('selected_part');
			select_all_checkbox.removeClass('selected_part').prop( "checked", false);
		}else{																													// else select all
			container.find("input[type=checkbox]").prop( "checked", true);
			select_all_wrapper.removeClass('selected_part');
			select_all_checkbox.prop( "checked", true);
		}
	}	
})(jQuery);
